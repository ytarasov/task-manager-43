package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.model.IUserOwnedService;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

}
