package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ytarasov.tm.api.service.IAuthService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.ytarasov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ytarasov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ytarasov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ytarasov.tm.dto.response.user.UserProfileResponse;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    private final IAuthService authService;

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.authService = serviceLocator.getAuthService();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String token = authService.login(login, password);
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        authService.logout(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final IUserServiceDTO userService = serviceLocator.getUserServiceDTO();
        @Nullable final UserDTO user = userService.findOneById(userId);
        return new UserProfileResponse(user);
    }

}
