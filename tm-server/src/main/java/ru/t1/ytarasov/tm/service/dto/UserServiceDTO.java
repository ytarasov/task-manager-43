package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.exception.user.ExistsEmailException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.repository.dto.UserRepositoryDTO;
import ru.t1.ytarasov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class UserServiceDTO
        extends AbstractServiceDTO<UserDTO, UserRepositoryDTO> implements IUserServiceDTO {

    @NotNull
    private final IPropertyService propertyService;

    public UserServiceDTO(@NotNull IConnectionService connectionService, @NotNull IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public UserDTO add(@Nullable UserDTO user) throws Exception {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public @NotNull Collection<UserDTO> add(@NotNull Collection<UserDTO> models) throws Exception {
        for (@NotNull final UserDTO model : models) add(model);
        return models;
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Boolean existsById;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            existsById = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @NotNull
    public UserDTO create(@Nullable final String login,
                          @Nullable final String password,
                          @Nullable final String email,
                          @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final String passwordHash = HashUtil.salt(propertyService, password);
        @NotNull final UserDTO user = new UserDTO(login, passwordHash, role);
        user.setEmail(email);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        @Nullable List<UserDTO> users;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            users = repository.findAll();
        } finally {
            entityManager.close();
        }
        return users;
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        UserDTO user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            user = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            user = repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            user = repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public int getSize() throws Exception {
        int size;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            size = repository.getSize();
        } finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    public UserDTO remove(@Nullable UserDTO user) throws Exception {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Nullable
    @Override
    public UserDTO removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return removeByLogin(user.getLogin());
    }

    @NotNull
    @Override
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    public void lockUser(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUser(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
