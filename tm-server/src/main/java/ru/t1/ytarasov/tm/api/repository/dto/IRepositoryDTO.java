package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IRepositoryDTO<M extends AbstractModelDTO> {

    void add(@NotNull final M model);

    void remove(@NotNull final M model);

    void update(@NotNull final M model);

    void clear();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull final String id);

    int getSize();

    Boolean existsById(@NotNull final String id);

}
