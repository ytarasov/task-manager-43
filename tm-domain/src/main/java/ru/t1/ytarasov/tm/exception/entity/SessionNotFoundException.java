package ru.t1.ytarasov.tm.exception.entity;

public final class SessionNotFoundException extends AbstractEntityNotFoundException {

    public SessionNotFoundException() {
        super("Error! SessionDTO not found...");
    }

}
