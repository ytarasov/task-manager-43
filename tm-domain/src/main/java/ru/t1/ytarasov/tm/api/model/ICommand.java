package ru.t1.ytarasov.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.io.IOException;

public interface ICommand {

    void execute() throws AbstractException, IOException, ClassNotFoundException;

    @Nullable
    String getName();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

}
