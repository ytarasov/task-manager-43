package ru.t1.ytarasov.tm.exception.field;

public final class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error! UserDTO's role is empty...");
    }

}
